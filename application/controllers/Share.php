<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Share extends CI_Controller {

    public function __construct()
    {   
            parent::__construct();   
            header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
             die();
        }        
            include APPPATH . 'libraries/classes/class.phpmailer.php';
            $this->load->helper('string');
            $this->load->helper(array('form', 'url'));
    }

    public function index(){
            
        $this->load->view('admin/ShareImage_view');   
    }

   public function sendMail()
    {   //echo "hi";exit();
        $msg= '<!DOCTYPE html>
        <html lang="en">
        <head>
          <title>Bootstrap Example</title>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <style>
          
             @font-face {
                        font-family: "phenomenabold";
                        src: url("http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/assets/fonts/phenomena-bold.woff2") format("woff2"),
                        url("http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/assets/fonts/phenomena-bold.woff") format("woff");
                        font-weight: normal;
                        font-style: normal;
                    }
            .container{
                background-color: #FFF !important;
                margin-top: 20px !important;
                border-radius: 10px !important;
                margin-bottom: 20px !important;
            }
            
            
            @media only screen and (max-width:381px) and (min-width: 320px){
                    .price{
                        float: left !important;
                    }
            }

            @media only screen and (max-width:568px) and (min-width: 320px){
                    .dy-width{
                       max-width: 285px;
                       min-width: 285px;
                    }
            }

            @media only screen and (max-width:667px) and (min-width: 375px){
                    .dy-width{
                       max-width: 285px;
                       min-width: 285px;
                    }
            }
            
            #body{
                 background-color: #45c4e4 !important;
            }
            .logo{
                margin-top: 40px;
            }
            
            .zoopla-img{
                margin-top: 10px;
                width: 100%;
                margin-left:0px !important;
                margin-right:0px !important;
            }
            
            .color-text{
                color: #45c4e4 !important;
                font-weight: bold;
                font-family: "phenomenabold";
            }
            
            
            table {
                width: auto;
                margin: 0 auto;
            }
            table td {
                float: left;
            }
          </style>
        </head>
        <body id="body">          
        <div class="container">
            <div class="row" style="margin-bottom: 40px;">
                <center>
                    <a href="http://theurbancollective.io" target="_blank">
                        <img class="logo" style="display:inline-block; line-height:0px; font-size:0px; border:0px;" src="http://ec2-35-177-13-64.eu-west-2.compute.amazonaws.com/uploads/share/logo.png" alt="img">
                    </a>
                </center>
            </div>
            <div class="row">
                <center>
                    <h1 style="color: #000;font-weight: 400 !important;margin-top: 2px !important;line-height: 0.9;font-family: "phenomenabold";">What do you think about this property?
                    </h1>
                </center>  
            </div> 

            <div class="row zoopla-img">
                <center>
                    <a href="" target="_blank">
                        <img class="img-responsive" src="https://li.zoocdn.com/358189500f59c4563e7ea6f350bcdc3cb9bfe145_354_255.jpg" style="border-radius: 10px;border: 3px solid #45c4e4;">
                    </a>
                </center>
            </div>  
            <!-- <center> -->
                <table cellpadding="0" cellspacing="0" style="margin-top:5px">
                  <tr>
                    <td valign="top"  class="dy-width" style="max-width: 269px;min-width: 269px;text-align:left;display: inline-flex;">
                       <span> <img src="http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/uploads/share/location1.png" style="height: 21px;">
                       </span>
                        <span class="color-text" style="padding-left: 5px;max-width:253px !important;word-wrap: break-word !important;">
                           Broadway,london SE1
                        </span>
                    </td>
                    <td>
                        <span class="color-text price" style="float:right;padding-left: 2px;"> Price:£1998 pcm</span>
                    </td>    
                  </tr>
                 
                </table>
            <!-- </center> -->
          
          <div style="margin-top:40px;margin-bottom:40px !important;">
            <center>
                <a href="https://itunes.apple.com/in/app/urbanco-rental-property-search/id1250754314?mt=8" target="_blank"> <img style="display:inline-block; line-height:0px; font-size:0px; border:0px;" src="http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/uploads/share/appstore.png" alt="img">
                </a>
            </center>
          </div>          
        </div>
        </body>
        </html>
';      
                  
        $msg = mb_convert_encoding($msg,"HTML-ENTITIES","UTF-8");   
          //echo $msg;exit();
          //include APPPATH . 'libraries/classes/class.phpmailer.php'; // include the class name                
                    $mail = new PHPMailer(true); // create a new object
                    $mail->CharSet = "UTF-8";
                    $mail->IsSMTP(); 
                    try{
                            $mail->IsHTML(true);        
                            $mail->SMTPDebug = 1;                                                        
                            $mail->Host = "smtp.googlemail.com"; //Hostname of the mail server  ssl://smtp.googlemail.com//smtpout.secureserver.net
                            $mail->Port = "587"; //Port of the SMTP like to be 25, 80, 465 or 587  ////465
                            $mail->SMTPAuth = true; //Whether to use SMTP authentication
                            $mail->Username = "bamgude.sachin@gmail.com"; //urbancotech@gmail.com Username for SMTP authentication any valid email created in your domain  bamgude.sachin@gmail.com
                            $mail->Password = "sbam@1991"; //97b-Th9-E7S-63c Password for SMTP authentication 
                            $mail->SMTPSecure  = 'tls'; 
                            $mail->SetFrom("urbancotech@gmail.com",'TheUrbanCollective');
                      //    $mail->SetFrom("bamgude.sachin@gmail.com");
                            $mail->Subject = "Property sharing";
                            $mail->Body = $msg;
                            $mail->AddAddress('saylisingh25@gmail.com');//whom to send mail urbancouk@gmail.com
                           // $mail->AddCC("");saylisingh25@gmail.com
                            $send = $mail->Send(); //Send the mails
                            //echo $mail->ErrorInfo;exit();
                            if($send){
                                 $this->response(array('ResponseCode' => 1, 'ResponseMessage' => 'SUCCESS', 'Comments' => 'MAIL SENT SUCCESFULLY','Result'=>$send), 200);
                            }else{
                                $this->response(array('ResponseCode' => 0, 'ResponseMessage' => 'FAILURE', 'Comments' => 'MAIL SENDING FAIL','Result'=>$send), 400);
                            }
                        } catch (phpmailerException $e) {
                          //echo $e->errorMessage();exit(); //Pretty error messages from PHPMailer
                            $this->response(array('ResponseCode' => 0, 'ResponseMessage' => 'FAILURE', 'Comments' => 'MAIL SENDING FAIL','Result'=>$send), 400);
                        
                        } catch (Exception $e) {
                          //echo $e->getMessage();exit(); //Boring error messages from anything else!
                            $this->response(array('ResponseCode' => 0, 'ResponseMessage' => 'FAILURE', 'Comments' => 'MAIL SENDING FAIL','Result'=>$send), 400);
                        }
    }


}