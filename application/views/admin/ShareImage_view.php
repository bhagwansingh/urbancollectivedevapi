<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Urban Collective</title>
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Fashion Newsletter templates, Email Templates, Newsletters, Marketing  templates, 
    Advertising templates, free Newsletter" />
<!-- //Custom Theme files -->
<!-- Responsive Styles and Valid Styles -->
<style type="text/css">     

@font-face {
                font-family: 'phenomenabold';
                src: url('http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/assets/fonts/phenomena-bold.woff2') format('woff2'),
                url('http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/assets/fonts/phenomena-bold.woff') format('woff');
                font-weight: normal;
                font-style: normal;
            }

    body{
        font-family: 'phenomenabold';
        width: 100%; 
        background-color: #fff; 
        margin:0; 
        padding:0; 
        -webkit-font-smoothing: antialiased;
    }
    html{
        font-family: 'phenomenabold';
        width: 100%; 
    }
    table{
        font-family: 'phenomenabold';
        font-size: 14px;
        border: 0;
    }
    /* ----------- Response ----------- */
    @media only screen and (max-width:640px){
        table[class=scale] {
            width: 100% !important;
        }
        td[class=hide] {
            display: none!important;
            height: 0px!important;
        }
        img.reset {
            margin: 0 auto;
        }
        td.title {
            text-align: center;
        }
        td[class=scale-center-both] {
            width: 100%!important;
            text-align: center!important;
            padding-left: 20px!important;
            padding-right: 20px!important;
        }
    }
    @media only screen and (max-width:480px){
        table.scale-full {
            width: 90%;
            text-align: center;
        }
        .table-grids {
            width: 65%;
            margin: 0 auto !important;
            float: none;
        }
        
    }
    @media only screen and (max-width:414px){
        table.scale-full {
            width: 94%;
        }
        td.banner {
            height: 250px; 
    
        }
        td.bnr-text {
            letter-spacing: 2px !important;
        }
        .table-grids {
            width: 75%;
        }
        table[class="table-full"] {
            text-align: center !important;
            float: none;
            margin: 0 auto;
        }
    }
    @media only screen and (max-width:375px){
        .table-grids {
            width: 82%;
        }
    }
    @media only screen and (max-width:320px){
        td.banner {
            height: 200px;
        }
        td.bnr-text {
            font-size: 18px !important;
            line-height: 31px !important;
        }
        .table-grids {
            width: 94%;
        }
    }
    p{font-family: 'phenomenabold';}
     /*@font-face {
                font-family: 'phenomenabold';
                src: url('http://localhost/urbanCollectiveDev/assets/fonts/phenomena-bold.woff2') format('woff2'),
                url('http://localhost/urbanCollectiveDev/assets/fonts/phenomena-bold.woff') format('woff');
                font-weight: normal;
                font-style: normal;
            }*/
    }
    
    
</style>  
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
    <table bgcolor="#45c4e4" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
            <tr>
                <td height="45" class="hide"></td>
            </tr>
            <tr>
                <td align="center">
                    <table width="600" style="border-radius: 10px !important;margin-bottom: 40px !important;" bgcolor="#fff" border="0" align="center" cellpadding="0" cellspacing="0" class="scale">
                        <tbody>
                            <tr>
                                <td height="20"></td>
                            </tr>
                            <!--logo-->
                            <tr>
                                <td class="logo" align="center" style="line-height: 0px;">
                                    <a href="http://theurbancollective.io" target="_blank" ><img style="display:inline-block; line-height:0px; font-size:0px; border:0px;" src="http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/uploads/share/logo.png" alt="img"></a>
                                </td>
                                
                            </tr>
                             <tr>
                                <td height="20"></td>
                            </tr>
                            <tr>
                                <td height="20" style="font-size: 40px !important;text-align: center;font-weight: 100; margin-top: 2px !important;font-family: 'phenomenabold'; line-height: 0.3;">
                                    <p>WHAT DO YOU THINK</p> 
                                    <p>ABOUT THIS PROPERTY?</p>
                                </td>
                            </tr>
                            </tr>
                            <!--end logo-->
                            
                            <!-- for loop code start here -->
                            
                            <!--//top-nav-->
                            <!--banner-->
                            <tr>
                                <td height="300" align="center" class="banner" >
                                    <center>
                                    <!-- zoopla property url -->
                                    <a href="http://theurbancollective.io" target="_blank" >
                                        <img src="https://li.zoocdn.com/358189500f59c4563e7ea6f350bcdc3cb9bfe145_354_255.jpg" style="width: 91%;border-radius: 10px;border: 3px solid #45c4e4;">
                                    </a>
                                    </center>
                                </td>
                            </tr>
                           <!-- <tr>
                                <td height="25"></td>
                            </tr>
                            -->
                            <!--//banner-->
                            <!--banner-bottom-->
                            <tr>
                                <td>
                                    <table width="540" border="0" cellspacing="0" cellpadding="0" align="center" class="scale-full">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <!--left-->
                                                    <table  align="left" style="" width="340" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody><tr>
                                                            <td height="0" align="left" style="margin-left:0px;">
                                                                <table  border="0" align="center" cellpadding="0" cellspacing="0" style="">
                                                                    <tbody>
                                                                        <tr>
                                                                        <td align="center" style="">
                                                                        <img style="height:21px;display:inline-block;" src="http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/uploads/share/location.png" alt="img">
                                                                        </td>
                                                                                
                                                                            <td style="font-family: 'phenomenabold'; font-weight: bold; color:#45c4e4; font-size:20px;">
                                                                                <!-- Property address -->
                                                                                <singleline label="Left Banner 1">High Street, St John'S Wood NW8 High Street, St John'S Wood NW8</singleline>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr></tbody>
                                                    </table>
                                                    <!--end left-->
                                                    <!--Space-->
                                                    
                                                    <!--End Space-->
                                                    <!-- agileits -->
                                                    <!--right-->
                                                    <table align="right" style=""  width="150" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                        <tr>
                                                            <td height="0" align="right" style="">
                                                                <table>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="right" style="font-family: 'phenomenabold'; font-weight: bold; color:#45c4e4; font-size:20px;float: right;">
                                                                                <!-- Property price -->
                                                                                <singleline label="Right banner 1">Price: $2145 PCM</singleline>
                                                                            </td>
                                                                        </tr>                                                                        
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <!--//end right-->
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <!--//banner-bottom-->
                            <!-- for loop end here -->
                            <tr>
                                <td height="50"></td>
                            </tr>
                            <!--latest-designs-->
                            <tr>
                                <td class="logo" align="center" style="line-height: 0px;">
                                   <a href="https://itunes.apple.com/in/app/urbanco-rental-property-search/id1250754314?mt=8" target="_blank"> <img style="display:inline-block; line-height:0px; font-size:0px; border:0px;" src="http://ec2-52-56-126-65.eu-west-2.compute.amazonaws.com/uploads/share/appstore.png" alt="img">
                                   </a>
                                </td>
                            </tr>
                            <tr>
                                <td height="40"></td>
                            </tr>
                            
        </tbody>
    </table>
    <tr>
                               
</body>
</html>